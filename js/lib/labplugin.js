const plugin = require('./index');
const base = require('@jupyter-widgets/base');

module.exports = {
  id: 'frontend_entity_selector_jupyter_widget',
  requires: [base.IJupyterWidgetRegistry],
  activate(app, widgets) {
      widgets.registerWidget({
          name: 'frontend_entity_selector_jupyter_widget',
          version: plugin.version,
          exports: plugin
      });
  },
  autoStart: true
};

