#!/usr/bin/env sh

while true; do
    read -p "Have you changed version in entity_selector_jupyter_widget/_version.py?[y/n] " yn
    case ${yn} in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo "cleaning env"
./dev.sh
echo "building package"
source venv/bin/activate
python3 setup.py sdist bdist_wheel
echo "uploading package"
twine upload -r testpypi dist/* || true
echo "done"
export pwver=`python3 -c "import entity_selector_jupyter_widget._version as v;print(v.__version__)"`
case ${pwver} in
        *[abrc]* ) echo "not final";;
        * ) echo "final"; twine upload dist/*;;
esac

#while true; do
#    read -p "Upload to PYPI?[y/n] " yn
#    case ${yn} in
#        [Yy]* ) twine upload dist/*; break;;
#        [Nn]* ) break;;
#        * ) echo "Please answer yes or no.";;
#    esac
#done
