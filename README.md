# entity_selector_jupyter_widget

A Jupyter Widget library for selecting entities in text

## Installation

To install run:
```bash
    $ pip install entity_selector_jupyter_widget
    $ jupyter nbextension enable --py --sys-prefix entity_selector_jupyter_widget
```

<!--To uninstall run:
```bash
    $ pip uninstall entity_selector_jupyter_widget
    $ jupyter nbextension disable entity_selector_jupyter_widget
    $ jupyter nbextension uninstall entity_selector_jupyter_widget
```-->

For a development installation (requires npm),
```bash
    $ git clone https://gitlab.com/randomunrandom/entity_selector_jupyter_widget.git
    $ cd entity_selector_jupyter_widget
    $ ./dev.sh
    ### it a utilitary script which deactivates virtual environment, creates new and launches jupyter notebook
```
[A development version is available via test.pypi](https://test.pypi.org/project/entity-selector-jupyter-widget/)

## Usage
See [example.py](https://gitlab.com/randomunrandom/entity_selector_jupyter_widget/blob/master/example.ipynb) for example of hw to use this extension.

## JupyterLab
Due to major differences between the Jupyter Notebook and JupyterLab, the extensions will not work in JupyterLab.

## License
It's an open source prject under MIT license
